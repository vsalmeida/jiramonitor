# Jira Monitor

-Grafana

-Prometheus

-Docker

## Installation - Prometheus Exporter For Jira

First you need to install the app below in your instance jira


https://marketplace.atlassian.com/apps/1217960/prometheus-exporter-for-jira


For more information

https://github.com/AndreyVMarkelov/jira-prometheus-exporter/wiki/Prometheus-Exporter-For-Jira

## Installation

1- Clone or download the project


```bash
git clone git@bitbucket.org:vsalmeida/jiramonitor.git
```


2- Change the prometheus configuration file


```bash
vi conf/prom/prometheus.yml
```


3- In the target field, include the host of your jira


```bash
 - job_name: 'jira'
    scheme: http # change to http if don't you have https
    metrics_path: '/plugins/servlet/prometheus/metrics'
    static_configs:
      - targets: ['your-jira-host:8080'] # Jira host and port you serve
```


4- Run the docker-compose


```bash
docker-compose -f jira-monitor.yml up
```



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
